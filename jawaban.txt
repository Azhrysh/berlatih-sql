1. buat data bases
create database myshop;

2.membuat table di dalam database
users
create table users(
    -> id int not null auto_increment key primary key,
    -> nama varchar(255) not null,
    -> email varchar(255) not null,
    -> password varchar(255) not null);

categories
create table categories(
    -> id int not null auto_increment key primary key,
    -> nama varchar(255) not null);

items
create table items(
    -> id int not null auto_increment key primary key,
    -> name varchar(255) not null,
    -> description varchar(255) not null,
    -> price int not null,
    -> stock int not null,
    -> category_id int,
    -> FOREIGN KEY (category_id) REFERENCES categories(id)
    -> );

3.Memasukkan Data pada Table
user
insert into users values(NULL, "Jhon Doe", "jhon@Doe.com", "jhon123");
insert into users values(NULL, "Jane Doe", "jane@Doe.com", "jenita123");

categories
insert into categories values(NULL, "gadget");
insert into categories values(NULL, "cloth");
insert into categories values(NULL, "men");
insert into categories values(NULL, "women");
insert into categories values(NULL, "branded");

items
insert into items values(NULL, "sumsangb50","hape keren dari merek sumsang", 4000000, 100, 1);
insert into items values(NULL, "Uniklooh","baju keren dari brand ternama", 500000, 50, 2);
insert into items values(NULL, "IMHO Watch","jam tangan anak yang jujur banget", 2000000, 10, 1);

4. mengambil data pada tabel
a) mengambil data table users kecuali kolom password
	select id, nama, email from users;

b) Mengambil data items
	select * from items where price > 1000000;
	select * from items where name like "uniklo";

c) Menampilkan data items join dengan kategori
select * from items inner join categories on items.category_id=categories.id;

5. Mengubah Data dari Database
 UPDATE  items  SET  price = 2500000 WHERE name = "Samsungb50";

 